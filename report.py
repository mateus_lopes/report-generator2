from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import cm, mm, inch
from datetime import datetime
import sys

def ler_arq(filename, dir2save):
    num_linhas = sum(1 for l in open(filename)) #open file to count the lines
    arq = open(filename, 'r') #open file to read the lines
    lista = [[0]*num_linhas for i in range(num_linhas-4)] #list initialization
    i = 0
    for linha in arq:
        if linha[6:8] == '13': #verify if line is a header or actually data
            lista[i][0] = linha[24:28]
            lista[i][1] = linha[36:43]
            lista[i][2] = linha[43:93]
            lista[i][3] = linha[127:134] #take some data from determined position in the verified line

            if lista[i][3][0:1] == '0':
                lista[i][3] = '  ' + lista[i][3][1:]
                if lista[i][3][2:3] == '0':
                    lista[i][3] = '    ' + lista[i][3][3:]
            lista[i][3] = 'R$' + lista[i][3]
            decimais = ',' + lista[i][3][-2:]
            lista[i][3] = lista[i][3][:-2] + decimais #adjust identation for salary column

            i = i+1
        else:
            pass
    gerar_pdf(i, lista, dir2save) #send the list to the function that generates the pdf

def gerar_pdf(qtde_linhas, lista, dir2save):
    data = datetime.now()
    nome = dir2save+'\\relatorio {}-{}-{} {}h{}.pdf'.format(data.day, data.month, data.year, data.hour, data.minute)

    pdf = Canvas(nome, pagesize = A4, bottomup = 0) #file's name and paper type 

    pdf.setFont("Helvetica", 32) #title's font
    pdf.setStrokeColorRGB(1, 0, 0)
    pdf.drawString(20, 50, "Relatório de pagamentos") #title's position and content

    pdf.setFont("Helvetica", 12) #content's font

    pdf.drawString(20, 90, "Agência")
    pdf.drawString(80, 90, "Conta corrente")
    pdf.drawString(180, 90, "Nome")
    pdf.drawString(500, 90, "Salário") #writes the header

    i2 = 0
    i3 = 0

    for i in range(qtde_linhas):
    	i3 = 115+25*i2
    	pdf.drawString(20, i3, lista[i][0]) #writes the rest of the lines skipping 25px for each one
    	pdf.drawString(80, i3, lista[i][1])
    	pdf.drawString(180, i3, lista[i][2])
    	pdf.drawString(500, i3, lista[i][3])
    	pdf.drawString(450, i3, str(i3))
    	i2 = i2+1
    	if i3>800:
    		i2=0
    		pdf.showPage() #next page, writes the header again, etc
    		pdf.drawString(20, 90, "Agência")
    		pdf.drawString(80, 90, "Conta corrente")
    		pdf.drawString(180, 90, "Nome")
    		pdf.drawString(500, 90, "Salário")

    pdf.showPage()
    pdf.save()
