#report-generator
================

Generate PDF report from a text file, using Python.
This project uses the reportlab library to generate PDF.
I've included an example file with random data for testing (example.txt)

####Changelog:

#####v0.1 (04/11/2014):
  *The project reads an .txt file and generates Pdf report.
  
  *The name of the file to be read is hardcoded.

#####v0.2 (07/11/2014):
  *The name of the file now must be sent through argument in the command line.

#####v0.3 (04/12/2014):
  *Now there's a minimal graphical module to use the report generator.
  
  *You can't use the terminal to pass the name of the file anymore, it must be selected through the file dialog.
  
#####v0.4 (12/12/2014):
  *GUI updated to make it more user-friendly.
