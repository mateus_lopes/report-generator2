from tkinter import *
from tkinter.filedialog import askopenfilename as ofd
from tkinter.filedialog import askdirectory as dirchoose
from tkinter.messagebox import *
from report import ler_arq, gerar_pdf

root = Tk()
root.wm_title("txt to PDF")
s_width = root.winfo_screenwidth()
s_height = root.winfo_screenheight()
w_width = 326
w_height = 300
root.geometry(str(w_width)+'x'+str(w_height)+'+'+str((s_width-w_width)/2).rstrip('0').rstrip('.')+'+'+str((s_height-w_height)/2).rstrip('0').rstrip('.'))

def getpath():
    filename = ofd(parent=root)
    if not filename:
        w = showinfo("Cancelado", "Operação cancelada pelo usuário!")
    else:
        v = filename
        w = showinfo("Salvar", "Escolha o local para salvar o relatório PDF:")
        saving = dirchoose()
        if not saving:
            w = showinfo("Cancelado", "Operação cancelada pelo usuário")
        else:
            ler_arq(filename, saving)
            w = showinfo("Sucesso", "Relatório salvo com sucesso!")

def sair():
    root.destroy()

lbl = Label(root, text = "Escolha o arquivo .txt para gerar o relatório:")
btabrir = Button(root, text = "Abrir...", command = getpath)
btsair = Button(root, text = "Sair", command = sair)

lbl.place(relx=0.05, rely=0.05)
btabrir.place(relx=0.05, rely=0.18, width=70)
btsair.place(relx=0.75, rely=0.85, width=70)
root.mainloop()